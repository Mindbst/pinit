# Import date class from datetime module 
from datetime import date

#begin interaction
date_today = date.today()
print('Good morning todays date is ' + str(date_today))
print('')
myName = input('What is your name?')
print('')
print('It is good to meet you, ' + myName)
print('')
myAge = int(input('Please enter your age.'))
print('')
#outcome of interaction
if int(myAge) > 35:
    print ('You are ' + str(int(myAge) - 35) + ' years older than me and obviously too old to understand.')
elif int(myAge) == 35:
    print ('You are the same age as me, but still not good enough!')
else:
    print ('You are ' + str(int(35) - int(myAge)) + ' years younger then me and too young to fathom the depths of my thinking.')
print('')
#when any key is pressed it will jump to sys.exit() 
input('Press any key and hit enter to exit.')
#sys.exit() is used to make the program quit
sys.exit()
